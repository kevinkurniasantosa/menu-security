import React from 'react';
import { Alert, ScrollView, TouchableOpacity, StyleSheet, Text, View, Image, ImageBackground } from 'react-native';
import Home from './components/Home'
import Navbar from './components/Navbar'
// import Settings from './components/Settings'

var {vw, vh, vmin, vmax} = require('react-native-viewport-units');

export default class App extends React.Component {
  render() {

    return (
      <ScrollView style={styles.container}>
        <Home />

        { /*SIDEBAR gw displayny gw ilangin dlu mw adain di css ny width ama height ny munculin*/ }

        <Navbar />
        
        {/* <Settings /> */}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  // menuBar: {
  //   flex: 1,
  //   backgroundColor: 'grey', // NTR DIGANTI
  //   width: 55*vw,
  //   position: 'absolute',
  //   left: 0,
  //   top: 0,
  //   minHeight: 100*vh,

  //   /* DI REACT NATIVE GK BSA DISPLAY NONE KLO MW ADAIN ILANGIN INI */
  //   // width: 0,
  //   // height: 0,
  // },

  // menuWrapper: {
  //   paddingTop: 15*vw,
  // },

  // profileImg: {
  //   width: 25*vw,  
  //   height: 25*vw,
  //   marginBottom: 2*vw,
  // },

  // profile: {
  //   alignItems: 'center',
  // },

  // namaProfile: {
  //   fontSize: 4.7*vw,
  //   margin: 0,
  //   paddingTop: 2.5*vw,
  // },

  // editProfile: {
  //   fontSize: 3*vw,
  //   margin: 0,
  //   paddingTop: 1.8*vw,
  //   paddingBottom: 9*vw,
  //   color: 'white',
  // },

  // options: {
  //   paddingVertical: 5*vw,
  //   marginHorizontal: 4*vw,
  //   borderTopColor: 'black',
  //   borderTopWidth: 1,
  // },

  // optionsOption: {
  //   display: 'flex',
  //   flexDirection: 'row',
  //   alignItems: 'center',
  //   paddingBottom: 7*vw,
  //   paddingHorizontal: 2*vw,
  // },

  // optionImg: {
  //   width: 5*vw,
  //   height: 5*vw,
  //   marginRight: 1.7*vw,
  // },

  // optionText: {
  //   fontSize: 4.7*vw,
  // },

  // logoWrapper: {
  //   alignItems: 'center',
  //   paddingTop: 11*vw,
  // },

  // logoBeejay: {
  //   width: 25*vw,
  //   height: 25*vw,
  // },

  ///////////////////////////////// MAIN MENU

  // mainMenu: {
  //   minHeight: 100*vh,
  //   backgroundColor: '#252b37',
  // },

  // headWrapper: {
  //   // text-align: center;
  //   display: 'flex',
  //   backgroundColor: '#252b37',
  //   paddingTop: 6*vw,
  //   alignItems: 'center',
  // },

  // homeImg: {
  //   width: 70*vw,
  //   height: 70*vw,
  // },

  // headTitle: {
  //   fontSize: 6*vw,
  //   color: '#60b557',
  //   margin: 0,
  // },

  // mainContent: {
  //   display: 'flex',
  //   flexDirection: 'column',
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   marginHorizontal: 8*vw,
  //   marginTop: 8*vw,
  //   paddingTop: 4*vw,
  //   backgroundColor: 'white',
  // },

  // row1Content: {
  //   display: 'flex',
  //   paddingBottom: 4*vw,
  //   flexDirection: 'row',
  //   justifyContent: 'center',
  // },

  // row2Content: {
  //   display: 'flex',
  //   justifyContent: 'center',
  //   flexDirection: 'row',
  //   paddingBottom: 7*vw,
  // },

  // buttonContent: {
  //   // text-align: center;
  //   alignItems: 'center',
  //   paddingHorizontal: 3*vw,
  // },

  // btnImg: {
  //   width: 34*vw,
  //   height: 33*vw,
  // },

  // btnText: {
  //   fontSize: 4.5*vw,
  //   margin: 0,
  //   marginTop: 2*vw,
  //   color: '#0f6e6e',
  // },

});
