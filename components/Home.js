import React from 'react';
import { Alert, ScrollView, TouchableOpacity, StyleSheet, Text, View, Image, ImageBackground } from 'react-native';

var {vw, vh, vmin, vmax} = require('react-native-viewport-units');

export default class Home extends React.Component {
    render() {
        return(
            <View style={styles.mainMenu}>
                <View style={styles.mainMenuWrapper}>
                    <View style={styles.headWrapper}>
                        <Image style={styles.homeImg} source={require("../assets/0_Logo/HouseLogo1(2).png")} />
                        <Text style={styles.headTitle}>SECURITY IS ACTIVE</Text>
                    </View>
                    <View style={styles.mainContent}>
                        <View style={styles.row1Content}>
                            <TouchableOpacity onPress={()=> {Alert.alert('ke live camera');}}>
                                <View style={styles.buttonContent}>
                                    <Image style={styles.btnImg} source={require("../assets/0_Logo/VideoLogo1.png")} />
                                    <Text style={styles.btnText}>LIVE CAMERA</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=> {Alert.alert('ke sos');}}>
                                <View style={styles.buttonContent}>
                                    <Image style={styles.btnImg} source={require("../assets/0_Logo/SOSLogo.png")} />
                                    <Text style={styles.btnText}>SOS</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.row2Content}>
                            <TouchableOpacity onPress={()=> {Alert.alert('ke activate lock');}}>
                                <View style={styles.buttonContent}>
                                    <Image style={styles.btnImg} source={require("../assets/0_Logo/ActivateNowLogo.png")} />
                                    <Text style={styles.btnText}>ACTIVATE LOCK</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=> {Alert.alert('ke lock door');}}>
                                <View style={styles.buttonContent}>
                                    <Image style={styles.btnImg} source={require("../assets/0_Logo/LockDoorLogo.png")} />
                                    <Text style={styles.btnText}>LOCK DOOR</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainMenu: {
        minHeight: 100*vh,
        backgroundColor: '#252b37',
    },

    headWrapper: {
        // text-align: center;
        display: 'flex',
        backgroundColor: '#252b37',
        paddingTop: 6*vw,
        alignItems: 'center',
    },

    homeImg: {
        width: 70*vw,
        height: 70*vw,
    },

    headTitle: {
        fontSize: 6*vw,
        color: '#60b557',
        margin: 0,
    },

    mainContent: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 8*vw,
        marginTop: 8*vw,
        paddingTop: 4*vw,
        backgroundColor: 'white',
    },

    row1Content: {
        display: 'flex',
        paddingBottom: 4*vw,
        flexDirection: 'row',
        justifyContent: 'center',
    },

    row2Content: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        paddingBottom: 7*vw,
    },

    buttonContent: {
        // text-align: center;
        alignItems: 'center',
        paddingHorizontal: 3*vw,
    },

    btnImg: {
        width: 34*vw,
        height: 33*vw,
    },

    btnText: {
        fontSize: 4.5*vw,
        margin: 0,
        marginTop: 2*vw,
        color: '#0f6e6e',
    },
});