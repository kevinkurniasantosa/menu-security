import React from 'react';
import { Alert, ScrollView, TouchableOpacity, StyleSheet, Text, View, Image, ImageBackground } from 'react-native';
import Gallery from './Gallery'
import Settings from './Settings'
import About from './About'
import FAQ from './FAQ'
import ContactSupport from './ContactSupport'
import { Navigation } from 'react-native-navigation'
import PropTypes from 'prop-types'

var {vw, vh, vmin, vmax} = require('react-native-viewport-units');

export default class Navbar extends React.Component {
    
    static propTypes = {
        navigator: PropTypes.object,
        componentId: PropTypes.string
    };

    constructor(props) {
        super(props);
        this.keGallery = this.keGallery.bind(this);
    }

    // keGallery = () => Navigation.push({
    //     component: {
    //         name: 'Gallery',
    //     }
    // })
    keGallery() {
        Navigation.push(this.props.componentId, {
            component: {
              name: 'Gallery',
            }
        });
    }

    keSettings = () => Navigation.setRoot({
        root: {
            component: {
                name: "Settings"
            }
        }
    })

    keAbout = () => Navigation.setRoot({
        root: {
            component: {
                name: "About"
            }
        }
    })

    keFAQ = () => Navigation.setRoot({
        root: {
            component: {
                name: "FAQ"
            }
        }
    })

    keContactSupport = () => Navigation.setRoot({
        root: {
            component: {
                name: "ContactSupport"
            }
        }
    })

    componentDidMount() {
        
    }


    render() {
        return(
            <View style={styles.menuBar}>
                <View style={styles.menuWrapper}>
                    <View style={styles.profile}>
                        <Image style={styles.profileImg} source={require("../assets/LogoNgasal.png")} />
                        <Text style={styles.namaProfile}>Kevin Kurnia Santosa</Text>
                        <Text style={styles.editProfile}>EDIT PROFILE</Text>
                    </View>
                    <View style={styles.options}>
                        <View style={styles.optionsOption}>
                            <Image style={styles.optionImg} source={require("../assets/LogoNgasal.png")} />
                            <TouchableOpacity onPress={this.keGallery}>
                                <Text style={styles.optionText}>Saved Image</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.optionsOption}>
                            <Image style={styles.optionImg} source={require("../assets/LogoNgasal.png")} />
                            <TouchableOpacity onPress={()=> {Alert.alert('ke settings');}}>
                                <Text style={styles.optionText}>Settings</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.optionsOption}>
                            <Image style={styles.optionImg} source={require("../assets/LogoNgasal.png")} />
                            <TouchableOpacity onPress={()=> {Alert.alert('ke about');}}>
                                <Text style={styles.optionText}>About</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.optionsOption}>
                            <Image style={styles.optionImg} source={require("../assets/LogoNgasal.png")} />
                            <TouchableOpacity onPress={()=> {Alert.alert('ke FAQ');}}>
                                <Text style={styles.optionText}>FAQ</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.optionsOption}>
                            <Image style={styles.optionImg} source={require("../assets/LogoNgasal.png")} />
                            <TouchableOpacity onPress={()=> {Alert.alert('ke contact support');}}>
                                <Text style={styles.optionText}>Contact Support</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.logoWrapper}>
                        <Image style={styles.logoBeejay} source={require("../assets/LogoNgasal.png")} />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    menuBar: {
        flex: 1,
        backgroundColor: 'grey', // NTR DIGANTI
        width: 55*vw,
        position: 'absolute',
        left: 0,
        top: 0,
        minHeight: 100*vh,

        /* DI REACT NATIVE GK BSA DISPLAY NONE KLO MW ADAIN ILANGIN INI */
        width: 0,
        height: 0,
    },

    menuWrapper: {
        paddingTop: 15*vw,
    },

    profileImg: {
        width: 25*vw,  
        height: 25*vw,
        marginBottom: 2*vw,
    },

    profile: {
        alignItems: 'center',
    },

    namaProfile: {
        fontSize: 4.7*vw,
        margin: 0,
        paddingTop: 2.5*vw,
    },

    editProfile: {
        fontSize: 3*vw,
        margin: 0,
        paddingTop: 1.8*vw,
        paddingBottom: 9*vw,
        color: 'white',
    },

    options: {
        paddingVertical: 5*vw,
        marginHorizontal: 4*vw,
        borderTopColor: 'black',
        borderTopWidth: 1,
    },

    optionsOption: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 7*vw,
        paddingHorizontal: 2*vw,
    },

    optionImg: {
        width: 5*vw,
        height: 5*vw,
        marginRight: 1.7*vw,
    },

    optionText: {
        fontSize: 4.7*vw,
    },

    logoWrapper: {
        alignItems: 'center',
        paddingTop: 11*vw,
    },

    logoBeejay: {
        width: 25*vw,
        height: 25*vw,
    },
});