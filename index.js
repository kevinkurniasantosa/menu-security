import { Navigation } from "react-native-navigation";
import App from "./App";
import Gallery from './components/Gallery'
import Settings from './components/Settings'
import About from './components/About'
import FAQ from './components/FAQ'
import ContactSupport from './components/ContactSupport'

Navigation.registerComponent(`navigation.playground.WelcomeScreen`, () => App);
Navigation.registerComponent('Home', () => require('./components/Home').default);
Navigation.registerComponent('Navbar', () => require('./components/Navbar').default);

/* BLM PASTI */
Navigation.registerComponent('Gallery', () => require('./components/Gallery').default);
Navigation.registerComponent('Settings', () => require('./components/Settings').default);
Navigation.registerComponent('About', () => require('./components/About').default);
Navigation.registerComponent('FAQ', () => require('./components/FAQ').default);
Navigation.registerComponent('ContactSupport', () => require('./components/ContactSupport').default);

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      component: {
        name: "navigation.playground.WelcomeScreen"
      }

    //   component: {
    //     name: "Gallery"
    //   },
    //   component: {
    //     name: "Settings"
    //   },
    //   component: {
    //     name: "About"
    //   },
    //   component: {
    //     name: "FAQ"
    //   },
    //   component: {
    //     name: "ContactSupport"
    //   }
    }
  });
});